package in.arraylist;

import java.util.HashSet;
import java.util.Set;

public class EqualsHashCodeExample {

	public static void main(String[] args) {

		Set<Employee> emp = new HashSet<>();

		Employee emp1 = new Employee(1, "ABC", "INFOSYS");
		Employee emp2 = new Employee(1, "XYZ", "HCL");

		emp.add(emp1);
		emp.add(emp2);
		System.out.println(emp);
		System.out.println(emp.size());
	}
}
