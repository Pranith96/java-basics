package in.arraylist;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Stack;

public class QueueExample {

	public static void main(String[] args) {

		Queue<Integer> qq = new ArrayDeque<Integer>();
		
		qq.add(1);
		qq.add(2);
		qq.add(3);
		qq.add(4);
		qq.add(5);
		qq.add(6);
		qq.add(7);
		qq.remove();
		qq.remove();
		qq.remove();

		System.out.println(qq);
		Stack<Integer> stack = new Stack<>();
		
		stack.push(1);
		stack.push(2);
		stack.pop();
		
		System.out.println(stack);
	}

}
