package in.arraylist;

import java.util.ArrayList;
import java.util.List;

import in.maps.Student;

public class ArrayListExample {

	public static void main(String[] args) {

		List<Integer> list = new ArrayList<>();

		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);

		System.out.println(list);

		int i = list.get(2);
		System.out.println(i);

		for (Integer l : list) {
			System.out.println(l);
		}

		List<Integer> list1 = new ArrayList<>();

		list1.add(5);
		list1.add(6);
		list1.add(7);
		list1.add(8);

		System.out.println(list1);

		// add all example
		list.addAll(list1);

		System.out.println(list);
		System.out.println("--------------------------------------------------------------------------------------");
		/*
		 * Student student = new Student(); student.setId(1); student.setName("XYZ");
		 */

		Student student = new Student(1, "abc");

		Student student1 = new Student();
		student1.setId(3);
		student1.setName("ABC");
		List<Student> s = new ArrayList<>();

		s.add(student);
		s.add(student1);

		System.out.println(s);

	}

}
