package in.arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ArraySetListExample {

	public static void main(String[] args) {

		Set<Student> set = new HashSet<>();

		College college = new College();
		college.setCollegeId(1);
		college.setCollegeName("XYZ");

		Student student = new Student();
		student.setId(1);
		student.setName("ABC");
		student.setCollege(college);

		Student newStudent = new Student(1, "WEE", new College(1, "YUY"));

		set.add(student);
		set.add(newStudent);

		List<Student> list = new ArrayList<>(set);
		list.add(newStudent);
		list.add(student);
		
		//list.forEach(System.out::println);
		
		for(Student s: list) {
			System.out.println(s);
		}

		List<Student> list1 = Arrays.asList(new Student(1, "ABC", new College(1, "XYZ")),
				(new Student(1, "ABC", new College(1, "XYZ"))));

		Set<Student> set1 = new HashSet<>(list1);
		System.out.println(set1);

	}
}
