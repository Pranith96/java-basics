package in.maps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapExample {

	public static void main(String[] args) {

		Map<Integer, Student> map = new HashMap<>();
		Student student1 = new Student();
		student1.setId(3);
		student1.setName("ABC");

		map.put(1, student1);

		map.put(2, new Student(1, "XYZ"));

		System.out.println(map);

		System.out.println("----------------------------------------");

		Map<Student, String> map1 = new HashMap<>();
		map1.put(student1, "CVR");

		System.out.println(map1);
		System.out.println("----------------------------------------");

		Map<Integer, List<Student>> map3 = new HashMap<>();

		List<Student> s = new ArrayList<>();

		s.add(new Student(1, "ABC"));
		s.add(new Student(2, "ABC"));
		s.add(new Student(3, "ABC"));
		s.add(new Student(4, "ABC"));

		map3.put(1, s);

		System.out.println(map3);

		List<Student> ss = map3.get(1);

		System.out.println(ss.get(1));

	}
}
